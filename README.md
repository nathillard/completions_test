# ChatGPT Completions Test

This is a command line-based test of the [ChatGPT Completions API](https://platform.openai.com/docs/api-reference/completions).

It enables you to pass a file containing a list of prompts, as well as a tsv file containing a list of article texts, and get back a file containing the completions for each prompt for each article text.

## Installation

This project uses [Poetry](https://python-poetry.org/) for dependency management. To install Poetry, run the following command:

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

Then, to install the dependencies, run the following command:

```bash
poetry install
```

## Setup

To use this project, you will need to set up an OpenAI account and create an API key. You can do this by following the instructions [here](https://platform.openai.com/account/api-keys).

Once you have this, copy the file `.env.example` to `.env` and replace the placeholder text with your API key. This file is ignored by git, so you don't have to worry about accidentally committing your API key.

## Usage

To run the script, run the following command:

```bash
poetry run cli --help
```

This will show you the subcommands available to call. There are three subcommands: `costs`, `summarize-section`, and `completions-from-file`.

### Costs

The `costs` subcommand will calculate the cost of provided input and output text according to certain models. It is primarily intended for debug purposes.

To use it, run the following command:

```bash
poetry run cli costs --input-text "hello world" --output-text "hi to you too"
```

* The `input-text` should be the text to use as the input to the model. It defaults to `hello world`.
* The `output-text` should be text representing the output from the model. It defaults to `hi to you too`.

### Summarize

The `summarize` subcommand will generate a summary of various kinds of text.

#### Summarize Section

The `summarize section` subcommand will generate a summary of an article and section supplied directly from the user.

To use it, run the following command:

```bash
 poetry run cli summarize section --article-title "Wikipedia" --section-title "Related projects"
```

* The `article-title` should be the title of the article to summarize. It defaults to `Wikipedia`.
* The `section-title` should be the title of the section to summarize. It defaults to `Related projects`.

#### Summarize File

The `summarize file` subcommand will generate completions for a list of prompts and a list of article texts. It will generate a completion for each prompt for each article text, and write the results to a file.

To use it, run the following command:

```bash
poetry run cli summarize file --prompt-file prompts.txt --article-file articles.tsv --output-file output.tsv
```

* The `prompt-file` should be a text file containing a list of prompts, one per line. It defaults to `prompts.txt`.
* The `article-file` should be a tsv file containing a list of article texts, with the first column containing the article text and the second column containing the article title. It defaults to `articles.tsv`.
* The `output-file` should be the path to the file to write the completions to. It defaults to `output.tsv`.

#### Summarize free-text

The `summarize free-text` subcommand will take free text from the user, and determine based on this which article and section to summarize.

To use it, run the following command:

```bash
poetry run cli summarize free-text "tell me about the personal life of Alyssa Thompson"
```
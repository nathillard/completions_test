""" Settings for the app. """

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    """ Settings for the app. Reads from .env file by default. """
    model_config = SettingsConfigDict(env_file='.env',
                                      env_file_encoding='utf-8')
    openai_api_key: str

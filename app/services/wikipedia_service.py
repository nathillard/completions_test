""" Module for working with the Wikipedia API. """

import logging

import requests

USER_AGENT = "GPTCompletions/v1.0 (nhillard@wikimedia.org)"
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


class WikipediaService:
    """ Service for interacting with the Wikipedia API. """
    def __init__(self, language="en"):
        self.language = language
        self.base_url = f"https://{self.language}.wikipedia.org/w/api.php"
        self.headers = {'User-Agent': USER_AGENT}

    def _make_request(self, params):
        resp = requests.get(self.base_url, params=params, timeout=100, headers=self.headers)
        resp.raise_for_status()
        return resp.json()

    def get_article_html(self, title):
        """ Get the text of a Wikipedia article. """
        params = {
            "action": "parse",
            "page": title,
            "prop": "text",
            "format": "json",
            "formatversion": 2
        }
        return self._make_request(params).get("parse", {}).get("text", "").get("*", "")

    def get_section_list(self, title):
        """ Get a list of sections of a Wikipedia article. """
        params = {
            "action": "parse",
            "page": title,
            "prop": "sections",
            "format": "json",
            "disabletoc": 1
        }
        return self._make_request(params).get("parse", {}).get("sections", [])

    def get_section_text(self, article_title: str, section_title: str):
        """ Get the text of a section of a Wikipedia article. """
        sections = self.get_section_list(article_title)
        section_index = None

        for section in sections:
            if section["line"] == section_title:
                section_index = section["index"]
                break

        if section_index is None:
            raise ValueError(f"Section '{section_title}' not found in article '{article_title}'.")

        params = {
            "action": "parse",
            "page": article_title,
            "prop": "wikitext",
            "format": "json",
            "section": section_index,
            "disabletoc": 1
        }

        return self._make_request(params).get("parse", {}).get("wikitext", "").get("*", "")


def main():
    """ Example usage of the WikipediaService class. """
    wiki_service = WikipediaService()

    # Get full article text
    article_text = wiki_service.get_article_html("House")
    logger.info("Article Text: %s", article_text)

    # Get sections of an article
    section_list = wiki_service.get_section_list("House")
    logger.info("Section List: %s", section_list)

    # Get text of a specific section
    section_text = wiki_service.get_section_text("House", "Etymology")
    logger.info("Section Text: %s", section_text)


if __name__ == "__main__":
    main()

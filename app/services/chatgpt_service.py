""" Test OpenAI ChatGPT completions. """
import logging

import openai

from app.constants import GPTModel
from app.models.data_function import DataFunction

logger = logging.getLogger(__name__)


class ChatGPTService:
    """ Service for interacting with the OpenAI ChatGPT API. """

    def prompt(self,
               model: GPTModel,
               messages: list[dict[str, str]] | None = None,
               user_text: str | None = None,
               system_text: str | None = None,
               functions: list[DataFunction] | None = None) -> openai.ChatCompletion:
        """ Send a prompt to ChatGPT. """
        try:
            extra_args = {}
            if functions is not None:
                extra_args["functions"] = functions

            messages = messages or []
            if messages == []:
                if system_text is not None:
                    messages.append({"role": "system",
                                    "content": system_text})
                messages.append({"role": "user",
                                "content": user_text})

            response = openai.ChatCompletion.create(
                model=model,
                messages=messages,
                temperature=0,
                **extra_args  # Unpack and add any extra arguments here
            )
            return response
        except openai.error.OpenAIError as openai_error:
            logger.error(openai_error)
            return None

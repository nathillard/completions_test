""" Service for coordinating the conversation workflow. """
import json

from app.constants import (ARTICLE_TITLE_PARAMETER_NAME,
                           GET_SECTION_TEXT_FUNCTION_NAME,
                           SECTION_TITLE_PARAMETER_NAME, GPTModel)
from app.services.chatgpt_service import ChatGPTService
from app.services.wikipedia_service import WikipediaService


class ChatCoordinatorService:
    """ Service for coordinating the conversation workflow. """
    def __init__(self):
        """
        Initialize the ChatCoordinatorService.

        :param chat_service: An instance of ChatGPTService for ChatGPT interactions.
        :param wikipedia_service: An instance of WikipediaService for fetching Wikipedia data.
        """
        self.chatgpt_service = ChatGPTService()
        self.wikipedia_service = WikipediaService()

    def handle_free_text(self,
                         system_text: str,
                         user_text: str,
                         model: GPTModel):
        """
        Handle the free text workflow starting with an initial user message.

        :param initial_message: The initial message from the user.
        :return: Final response from ChatGPT after handling any necessary function calls.
        """
        messages = [{"role": "system", "content": system_text},
                    {"role": "user", "content": user_text}]

        # Step 1: Start the conversation with ChatGPT.
        initial_response = self.chatgpt_service.prompt(
            user_text=user_text,
            model=model,
            functions=self.function_specifications
        )

        # Check for function call request in the response.
        response_message = initial_response.choices[0].message

        # Step 2: check if GPT wanted to call a function
        if identified_function_call := response_message.get("function_call"):
            # Step 3: call the function
            identified_function_name = identified_function_call.name
            identified_function_args = identified_function_call.arguments
            args = json.loads(identified_function_args)

            function_to_call = self.function_name_mapping.get(identified_function_name)
            function_response = function_to_call(
                article_title=args[ARTICLE_TITLE_PARAMETER_NAME],
                section_title=args[SECTION_TITLE_PARAMETER_NAME]
            )

            # Step 4: send the info on the function call and function response to GPT
            messages.append(response_message)  # extend conversation with assistant's reply
            messages.append(
                {
                    "role": "function",
                    "name": identified_function_name,
                    "content": function_response,
                }
            )  # extend conversation with function response

            second_response = self.chatgpt_service.prompt(
                messages=messages,
                model=model
            )
            return second_response
        return initial_response

    @property
    def function_name_mapping(self):
        """ Return a mapping of function names to their implementations. """
        return {
            GET_SECTION_TEXT_FUNCTION_NAME: self.wikipedia_service.get_section_text
        }

    @property
    def function_specifications(self):
        """
        Return a list of available functions with their definitions.

        :return: List of available functions.
        """
        get_section_function = {
            "name": GET_SECTION_TEXT_FUNCTION_NAME,
            "description": "Get the text of a specific section.",
            "parameters": {
                "type": "object",
                "properties": {
                    ARTICLE_TITLE_PARAMETER_NAME: {
                        "type": "string",
                        "description": "The title of the Wikipedia article.",
                    },
                    SECTION_TITLE_PARAMETER_NAME: {
                        "type": "string",
                        "description": "The title of the section.",
                    }
                },
                "required": [ARTICLE_TITLE_PARAMETER_NAME, SECTION_TITLE_PARAMETER_NAME],
            }
        }
        functions = [
            get_section_function
        ]
        return functions

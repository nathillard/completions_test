""" Calculate the cost of a given string for a given model. """

import logging

import tiktoken

from .constants import GPTModel

logger = logging.getLogger(__name__)


def cost_for_token_count(model: GPTModel,
                         input_token_count: int,
                         output_token_count: int | None = None) -> float:
    """
    Calculate the cost for using ChatGPT API.

    Args:
    - model (GPTModel): The model enumeration. Options: GPTModel.GPT4, GPTModel.GPT3_5_TURBO, GPTModel.BABBAGE.
    - input_tokens (int): Number of input tokens.
    - output_tokens (int): Number of output tokens.

    Returns:
    - float: The cost in USD.
    """

    costs = {
        GPTModel.GPT4: {
            8: {'input': 0.03, 'output': 0.06},
            32: {'input': 0.06, 'output': 0.12}
        },
        GPTModel.GPT3_5_TURBO: {
            4: {'input': 0.0015, 'output': 0.002},
            16: {'input': 0.003, 'output': 0.004}
        },
        GPTModel.BABBAGE: {
            None: {'input': 0.0004, 'output': 0.0004}
        }
    }

    # Determine context size based on number of input tokens
    if model == GPTModel.GPT4:
        context = 8 if input_token_count <= 8000 else 32
    elif model == GPTModel.GPT3_5_TURBO:
        context = 4 if input_token_count <= 4000 else 16
    else:
        context = None

    # Convert token counts to the cost per 1K tokens
    input_cost_per_token = costs[model][context]['input'] / 1000
    output_cost_per_token = costs[model][context]['output'] / 1000

    # Calculate the total cost
    total_cost = input_token_count * input_cost_per_token
    if output_token_count:
        total_cost += (output_token_count * output_cost_per_token)

    return total_cost


def count_tokens(text: str, model: GPTModel) -> int:
    """ Count the number of tokens in a given text
    according to a given model. """
    enc = tiktoken.encoding_for_model(model)
    encoded_text = enc.encode(text)
    num_tokens = len(encoded_text)
    logger.debug("Encoded Text Length: %s", num_tokens)
    return num_tokens


def cost_for_text(text: str, model: GPTModel) -> float:
    """ Calculate the cost for a given text and model. """
    num_tokens = count_tokens(text, model)
    return cost_for_token_count(model=model,
                                input_token_count=num_tokens)

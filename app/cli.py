""" Command line interface for app. """

import csv
import logging
from typing import Annotated

import openai
import typer
from tabulate import tabulate

from app.constants import GPTModel
from app.services.chatgpt_service import ChatGPTService
from app.services.wikipedia_service import WikipediaService
from app.services.chat_coordinator_service import ChatCoordinatorService
from app.settings import Settings
from app.tokenize import cost_for_token_count, count_tokens

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())

settings = Settings()

app = typer.Typer()
summarize_app = typer.Typer()
app.add_typer(summarize_app, name="summarize", help="Summarize a given text.")


def get_costs(response: openai.ChatCompletion, gpt_model: GPTModel) -> tuple:
    """ Compute the cost for a given response. """
    input_token_count = response.usage.prompt_tokens
    output_token_count = response.usage.completion_tokens
    num_tokens = response.usage.total_tokens
    cost = cost_for_token_count(model=gpt_model,
                                input_token_count=input_token_count, output_token_count=output_token_count)
    logger.debug("""Input tokens: %s,
                Output tokens: %s,
                Total tokens: %s,
                cost: %.6f""", input_token_count, output_token_count, num_tokens, cost)
    return input_token_count, output_token_count, cost


@app.command()
def costs(input_text: Annotated[
                    str,
                    typer.Option(help="Input text to estimate costs for")
                ] = "hello world",
          output_text: Annotated[
                    str,
                    typer.Option(help="Output text to estimate costs for")
                ] = None):
    """ Estimate costs for a given token. """
    logger.info("""Provided text:
                Input Text: \"%s\",
                Output Text: \"%s\"""", input_text, output_text)

    # Prepare data for the table
    model_names = []
    input_token_counts = []
    output_token_counts = []
    all_costs = []

    # Loop through all GPTModel values and compute cost for each
    for model in GPTModel:
        logger.debug("Original text: \"%s\"", input_text)
        model_names.append(model)

        input_token_count = count_tokens(input_text, model)
        input_token_counts.append(input_token_count)
        output_token_count = count_tokens(output_text, model) if output_text else 0
        output_token_counts.append(output_token_count)

        cost = cost_for_token_count(model=model,
                                    input_token_count=input_token_count, output_token_count=output_token_count)
        all_costs.append(cost)

    # Generate the table using tabulate
    headers = ["Model", "Input Token Count", "Output Token Count", "Cost"]
    table = list(zip(model_names, input_token_counts, output_token_counts, all_costs))
    logger.info("\n %s", tabulate(table, headers=headers, floatfmt=".6f"))


@summarize_app.command()
def section(article_title: Annotated[
                        str,
                        typer.Option(help="The title of the Wikipedia article to use")
                    ] = "Wikipedia",
            section_title: Annotated[
                        str,
                        typer.Option(help="The title of the section to summarize")
                    ] = "Related projects"):
    """ Summarize a section of a Wikipedia article. """
    openai.api_key = settings.openai_api_key
    chatgpt_service = ChatGPTService()
    logger.info("Querying Wikipedia for article \"%s\" and section \"%s\"...", article_title, section_title)
    wiki_service = WikipediaService()
    section_text = wiki_service.get_section_text(article_title, section_title)
    system_prompt = "Provide a 100 word summary of the supplied text that is understandable to a 10 year old."
    logger.info("Querying ChatGPT...")
    logger.info("Request: \"%s\"", system_prompt + section_text)
    gpt_model = GPTModel.GPT3_5_TURBO
    response = chatgpt_service.prompt(system_text=system_prompt,
                                      user_text=section_text,
                                      model=gpt_model)
    (input_token_count, output_token_count, cost) = get_costs(response, gpt_model)
    response_text = response.choices[0].message.content
    logger.info("Response: \"%s\"", response_text)
    logger.info("Input tokens: %s, Output tokens: %s, cost: %.6f", input_token_count, output_token_count, cost)


@summarize_app.command()
def free_text(text: Annotated[
                    str,
                    typer.Argument(help="Free text to parse and summarize")
                ] = "What does the history section of the Wikipedia article on the United States say?"):
    """ Summarize provided text by finding the appropriate article and section of a wiki page"""
    openai.api_key = settings.openai_api_key

    chat_coordinator_service = ChatCoordinatorService()
    system_text = """Provide a 100 word summary of the supplied text that
                        is understandable to a 10 year old."""
    response = chat_coordinator_service.handle_free_text(system_text=system_text,
                                                         user_text=text,
                                                         model=GPTModel.GPT3_5_TURBO)
    (input_token_count, output_token_count, cost) = get_costs(response, GPTModel.GPT3_5_TURBO)
    response_text = response.choices[0].message.content
    logger.info("Response: \"%s\"", response_text)
    logger.info("Input tokens: %s, Output tokens: %s, cost: %.6f", input_token_count, output_token_count, cost)


@summarize_app.command()
def file(prompt_file: Annotated[
                    str,
                    typer.Option(help="The file containing the prompts to use")
                ] = "prompts.txt",
         article_file: Annotated[
                    str,
                    typer.Option(help="The file containing the article text to use")
                ] = "articles.tsv",
         output_file: Annotated[
                    str,
                    typer.Option(help="The file to write the results to")
                ] = "output.tsv"):
    """ Get completions for a given prompt. """
    openai.api_key = settings.openai_api_key
    chatgpt_service = ChatGPTService()
    system_prompts = []
    with open(prompt_file, "r", encoding="utf-8") as prompt_handle:
        system_prompts = [line.strip() for line in prompt_handle.readlines() if line.strip()]

    article_texts = []
    with open(article_file, "r", encoding="utf-8") as article_handle:
        reader = csv.DictReader(article_handle, delimiter="\t")
        for row in reader:
            article_texts.append(row['Excerpt'])
    logger.info("Querying ChatGPT...")
    responses = []
    gpt_model = GPTModel.GPT3_5_TURBO

    for i, article_text in enumerate(article_texts):
        logger.info("Processing article %s...", i)
        for j, system_prompt in enumerate(system_prompts):
            logger.info("Processing prompt %s...", j)
            response = chatgpt_service.prompt(system_text=system_prompt,
                                              user_text=article_text,
                                              model=gpt_model)
            (input_token_count, output_token_count, cost) = get_costs(response, gpt_model)
            response_text = response.choices[0].message.content
            response = {"system_prompt": system_prompt,
                        "article_text": article_text,
                        "response": response_text,
                        "input_tokens": input_token_count,
                        "output_tokens": output_token_count,
                        "cost": cost}
            responses.append(response)

    logger.info("Writing to file %s", output_file)
    with open(output_file, "w", encoding="utf-8", newline='') as out_handle:
        fieldnames = responses[0].keys()
        writer = csv.DictWriter(out_handle, fieldnames=fieldnames, delimiter="\t")
        writer.writeheader()  # writes the field names as header
        for response in responses:
            writer.writerow(response)
    logger.info("Writing complete.")


if __name__ == "__main__":
    app()

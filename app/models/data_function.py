""" Data Function model for representing . """

from pydantic import BaseModel


class DataFunctionParameterProperty(BaseModel):
    """ A ChatGPT function-calling function parameter property. """
    type: str
    description: str | None = None


class DataFunctionEnumProperty(DataFunctionParameterProperty):
    """ A ChatGPT function-calling function parameter
    property with an enum. """
    enum: list[str] = None


class DataFunctionParameters(BaseModel):
    """ A ChatGPT function-calling function parameter. """
    type: str
    properties: dict[str, DataFunctionParameterProperty | DataFunctionEnumProperty]
    required: list[str]


class DataFunction(BaseModel):
    """ A ChatGPT function-calling function. """
    name: str
    description: str
    parameters: DataFunctionParameters

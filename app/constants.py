""" Constants for the app package. """

from enum import StrEnum


class GPTModel(StrEnum):
    """ The OpenAI Model to use. """
    BABBAGE = "babbage"
    GPT3_5_TURBO = "gpt-3.5-turbo"
    GPT4 = "gpt-4"


GET_SECTION_TEXT_FUNCTION_NAME = "get_section_text"
ARTICLE_TITLE_PARAMETER_NAME = "article_title"
SECTION_TITLE_PARAMETER_NAME = "section_title"

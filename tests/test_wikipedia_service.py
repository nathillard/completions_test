""" Test the WikipediaService class. """

from unittest.mock import patch

import pytest

from app.services.wikipedia_service import WikipediaService


@pytest.fixture(autouse=True)
def mock_wikipedia_service():
    """ Mock the WikipediaService class. """
    def mock_responses(params):
        if params["prop"] == "sections":
            return {
                "parse": {
                    "sections": [
                        {
                            "line": "Section 1",
                            "index": "1"
                        },
                        {
                            "line": "Section 2",
                            "index": "2"
                        }
                    ]
                }
            }
        elif params["prop"] == "wikitext":
            return {
                "parse": {
                    "title": "Title",
                    "pageid": 12345,
                    "wikitext": {
                        "*": "This is the section text."
                    }
                }
            }
        elif params["prop"] == "text":
            return {
                "parse": {
                    "title": "Title",
                    "pageid": 12345,
                    "text": {
                        "*": "<html>This is the article text.</html>"
                    }
                }
            }
        else:
            return {}
    with patch("app.wikipedia.WikipediaService._make_request") as mock_make_request:
        mock_make_request.side_effect = mock_responses
        yield mock_make_request


def test_get_article_html():
    """ Test the get_article_html method. """
    service = WikipediaService()
    article_text = service.get_article_html("Test article")
    assert article_text == "<html>This is the article text.</html>"


def test_get_section_list():
    """ Test the get_section_list method. """
    service = WikipediaService()
    sections = service.get_section_list("Test article")
    assert sections == [
        {
            "line": "Section 1",
            "index": "1"
        },
        {
            "line": "Section 2",
            "index": "2"
        }
    ]


def test_get_section_text():
    """ Test the get_section_text method. """
    service = WikipediaService()
    section_text = service.get_section_text("Test article", "Section 2")
    assert section_text == "This is the section text."


def test_get_section_text_not_found():
    """ Test the get_section_text method when the section is not found. """
    service = WikipediaService()
    with pytest.raises(ValueError):
        service.get_section_text("Test article", "Section 3")
